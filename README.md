# Holoblade

HoloBlade is an open-hardware driver-stack for Spatial Light Modulators (SLMs). Its primary application is for holographic displays and may be used in applied optics research fields as telecommunications, astronomy, microscopy and optical computing.